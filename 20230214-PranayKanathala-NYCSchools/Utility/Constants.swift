//
//  Constants.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation


// MARK: - Application URL's
struct APPURL {
    static let baseUrl = getBaseURL()
    static func getBaseURL() -> String {
        return "https://data.cityofnewyork.us/resource/"
    }
    static let displayHighSchools = "s3k6-pzi2.json"
    static let schoolDetails = "f9bf-2cp4.json"
}
// MARK: - HTTP Methods
enum HttpMethod: String {
    case get
    case post
    case put
    case patch
    case delete
}
// MARK: - Tableview Identifiers
enum TableViewCellIdentifiers: String {
    case HighSchoolTableViewCellIdentifier = "HighSchoolTableViewCellIdentifier"
}
