//
//  RestError.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation

struct RestError {
    let statusCode: Int
    let statusMessage: String
    init(_ code: Int, _ msg: String) {
        self.statusCode = code
        self.statusMessage = msg
    }
}
struct LocationError {
    let errorMessage: String
    init(_ msg: String) {
        self.errorMessage = msg
    }
}
