//
//  RestClientConstants.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation

typealias APIMethod = HttpMethod

typealias APIHeaders = [String: String]
typealias APIParameter = [String: Any]?
typealias APIErrorHandler = (RestError) -> Void
typealias LocationErrorHandler = (LocationError) -> Void


enum RestClientConstants {
    static let reqTimeOutSec: TimeInterval = 300.0
}


enum APIHeadersType {
    static let accept: String = "accept"
    static let content: String = "Content-Type"
    static let json: String = "application/json"
    static let Authorization: String = "Authorization"
    static let urlEncoded: String = "application/x-www-form-urlencoded"
    
}
