//
//  DataManager.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation

class DataManager  {
    // MARK: - API call to fetch weather data

    class func getHighSchoolData(onSuccess: @escaping ([HighSchoolModel]) -> Void,
                               onError: @escaping APIErrorHandler) {
        let urlString = APPURL.getBaseURL() + APPURL.displayHighSchools
        guard let url = URL(string: urlString) else { return  }
        RestClient.defaultRestSession.makeRequest(toURL: url, withHttpMethod: HttpMethod.get, completion: { (results) in
            if let data = results.data {
                let decoder = JSONDecoder()
                guard let response = try? decoder.decode([HighSchoolModel].self, from: data) else { return }
                onSuccess(response)
            } else {
                if let statuscode = results.response?.httpStatusCode {
                    let errorData = RestError(statuscode, results.error.debugDescription)
                    onError(errorData)
                } else {
                    let errorData = RestError(results.response?.httpStatusCode ?? 400, results.error.debugDescription)
                    onError(errorData)
                }
            }
        })
    }
    
    
    class func getHighSchoolDetailData(onSuccess: @escaping ([SchoolDetailModel]) -> Void,
                               onError: @escaping APIErrorHandler) {
        let urlString = APPURL.getBaseURL() + APPURL.schoolDetails
        guard let url = URL(string: urlString) else { return  }
        RestClient.defaultRestSession.makeRequest(toURL: url, withHttpMethod: HttpMethod.get, completion: { (results) in
            if let data = results.data {
                let decoder = JSONDecoder()
                guard let response = try? decoder.decode([SchoolDetailModel].self, from: data) else { return }
                onSuccess(response)
            } else {
                if let statuscode = results.response?.httpStatusCode {
                    let errorData = RestError(statuscode, results.error.debugDescription)
                    onError(errorData)
                } else {
                    let errorData = RestError(results.response?.httpStatusCode ?? 400, results.error.debugDescription)
                    onError(errorData)
                }
            }
        })
    }

}

