//
//  SchoolDetailModel.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation

struct SchoolDetailModel: Decodable {
    let dbn: String?
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
    enum DecodingKeys: String, CodingKey {
        case dbn,school_name, num_of_sat_test_takers, sat_critical_reading_avg_score, sat_math_avg_score,sat_writing_avg_score
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DecodingKeys.self)
        self.dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
        self.school_name = try container.decodeIfPresent(String.self, forKey: .school_name)
        self.num_of_sat_test_takers = try container.decodeIfPresent(String.self, forKey: .num_of_sat_test_takers)
        self.sat_critical_reading_avg_score = try container.decodeIfPresent(String.self, forKey: .sat_critical_reading_avg_score)
        self.sat_math_avg_score = try container.decodeIfPresent(String.self, forKey: .sat_math_avg_score)
        self.sat_writing_avg_score = try container.decodeIfPresent(String.self, forKey: .sat_writing_avg_score)
    }
}
