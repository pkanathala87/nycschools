//
//  SchoolDetailVC.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import UIKit

class SchoolDetailVC: BaseViewController {
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var testTakersLabel: UILabel!
    @IBOutlet weak var readingAverageLabel: UILabel!
    @IBOutlet weak var mathsAverageLabel: UILabel!
    @IBOutlet weak var writingAverageLabel: UILabel!
    @IBOutlet weak var mainStack: UIStackView!

    var viewModel: SchoolDetailViewModel?
     var selectedSchool: HighSchoolModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SchoolDetailViewModel()
        viewModel?.selectedSchool = self.selectedSchool
        self.getHighSchoolData()
    }
    
    private func bindData() {
        if let data = viewModel?.getSchoolData() {
            self.schoolName.text = data.school_name ?? ""
            self.testTakersLabel.text = data.num_of_sat_test_takers ?? ""
            self.readingAverageLabel.text = data.sat_critical_reading_avg_score ?? ""
            self.mathsAverageLabel.text = data.sat_math_avg_score ?? ""
            self.writingAverageLabel.text = data.sat_writing_avg_score ?? ""
        }
    }
    
    private func getHighSchoolData() {
        self.createSpinnerView()
        viewModel?.getSchoolDetails(onSuccess: {(data) in
            self.removeSpinnerView()
            DispatchQueue.main.sync {
                self.bindData()
            }
        }, onError: {(error) in
            self.removeSpinnerView()
            self.showAlert(title: "Alert", message: error.statusMessage)
        })
    }

}
