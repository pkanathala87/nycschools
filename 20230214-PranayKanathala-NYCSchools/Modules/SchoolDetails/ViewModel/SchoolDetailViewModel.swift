//
//  SchoolDetailViewModel.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation

class SchoolDetailViewModel {
    init(){}
    var schoolDetailModel: [SchoolDetailModel]?
    var selectedSchool: HighSchoolModel?
    func getSchoolDetails(onSuccess: @escaping ([SchoolDetailModel]) -> Void,
                           onError: @escaping APIErrorHandler) {
        if self.schoolDetailModel == nil {
            self.schoolDetailModel = [SchoolDetailModel]()
        }
       DataManager.getHighSchoolDetailData(onSuccess: { [weak self] (data) in
            self?.schoolDetailModel = data
            onSuccess(data)
        }) { (error) in
            onError(error)
        }
    }
    
    func getSchoolData() -> SchoolDetailModel? {
        guard let selectedSchool = selectedSchool else { return nil }
        if let detailData = self.schoolDetailModel?.filter({$0.dbn == selectedSchool.dbn}).first {
            return detailData
        }
        return nil
    }
    
}
