//
//  HighSchoolTableViewCell.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import UIKit

class HighSchoolTableViewCell: UITableViewCell {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var website: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: HighSchoolModel?) {
        guard let data = data else { return }
        self.schoolName.text = data.school_name ?? ""
        self.location.text = data.location ?? ""
        self.phoneNumber.text = data.phone_number ?? ""
        self.website.text = data.website ?? ""
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
