//
//  HighSchoolModel.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation

struct HighSchoolModel: Decodable {
    let dbn: String?
    let school_name: String?
    let location: String?
    let phone_number: String?
    let website: String?
    enum DecodingKeys: String, CodingKey {
        case dbn, school_name, location, phone_number,website
    }
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DecodingKeys.self)
        self.dbn = try container.decodeIfPresent(String.self, forKey: .dbn)
        self.school_name = try container.decodeIfPresent(String.self, forKey: .school_name)
        self.location = try container.decodeIfPresent(String.self, forKey: .location)
        self.phone_number = try container.decodeIfPresent(String.self, forKey: .phone_number)
        self.website = try container.decodeIfPresent(String.self, forKey: .website)
    }
}
