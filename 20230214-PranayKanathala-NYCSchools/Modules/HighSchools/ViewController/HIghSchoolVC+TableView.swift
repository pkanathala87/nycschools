//
//  HIghSchoolVC+TableView.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import UIKit

extension HighSchoolVC: UITableViewDelegate, UITableViewDataSource {
    func setTableview() {
        self.highSchoolTableView.delegate = self
        self.highSchoolTableView.dataSource = self
        self.highSchoolTableView.estimatedRowHeight = 300
        self.highSchoolTableView.rowHeight = UITableView.automaticDimension
        self.highSchoolTableView.tableFooterView = UIView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.highSchoolResults?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifiers.HighSchoolTableViewCellIdentifier.rawValue, for: indexPath) as UITableViewCell
        if let cell = cell as? HighSchoolTableViewCell {
            cell.selectionStyle = .none
            if let data = viewModel?.highSchoolResults?[indexPath.row] {
                cell.setData(data: data)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SchoolDetailVC") as? SchoolDetailVC {
            if let data = viewModel?.highSchoolResults?[indexPath.row] {
                viewController.selectedSchool = data
            }
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
