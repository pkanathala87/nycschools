//
//  HighSchoolVC.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import UIKit

class HighSchoolVC: BaseViewController {
    @IBOutlet weak var highSchoolTableView: UITableView!
    var viewModel: HighSchoolViewModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = HighSchoolViewModel()
        self.setTableview()
        self.getHighSchoolData()
        self.title = "NYC Schools"
        // Do any additional setup after loading the view.
    }
    
    private func getHighSchoolData() {
        self.createSpinnerView()
        viewModel?.getHighSchoolData(onSuccess: {(data) in
            self.removeSpinnerView()
            DispatchQueue.main.sync {
                self.refreshTableView()
            }
        }, onError: {(error) in
            self.removeSpinnerView()
            self.showAlert(title: "Alert", message: error.statusMessage)
        })
    }
    
    private func refreshTableView() {
        self.highSchoolTableView.reloadData()
    }

}
