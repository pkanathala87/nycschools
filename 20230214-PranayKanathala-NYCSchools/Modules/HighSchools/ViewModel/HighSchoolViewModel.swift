//
//  HighSchoolViewModel.swift
//  20230214-PranayKanathala-NYCSchools
//
//  Created by Pet on 2/14/23.
//

import Foundation
class HighSchoolViewModel {
    init(){}
    var highSchoolResults: [HighSchoolModel]?
    func getHighSchoolData(onSuccess: @escaping ([HighSchoolModel]) -> Void,
                           onError: @escaping APIErrorHandler) {
        if self.highSchoolResults == nil {
            self.highSchoolResults = [HighSchoolModel]()
        }
       DataManager.getHighSchoolData(onSuccess: { [weak self] (data) in
            self?.highSchoolResults = data
            onSuccess(data)
        }) { (error) in
            onError(error)
        }
    }
}
