//
//  LoadLocalData.swift
//  20230214-PranayKanathala-NYCSchoolsTests
//
//  Created by Pet on 2/14/23.
//

import XCTest

extension XCTestCase {
    // MARK: - Helper Methods
     func loadStub(name: String) -> Data? {
        let bundle = Bundle(for: type(of: self))
        if let url = bundle.url(forResource: name, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url, options: .mappedIfSafe)
               return data
            } catch {
                // Handle error here
            }
        }
        return nil
    }
 }
