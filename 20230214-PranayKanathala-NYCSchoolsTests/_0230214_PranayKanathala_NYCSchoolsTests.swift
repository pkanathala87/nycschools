//
//  _0230214_PranayKanathala_NYCSchoolsTests.swift
//  20230214-PranayKanathala-NYCSchoolsTests
//
//  Created by Pet on 2/14/23.
//

import XCTest
@testable import _0230214_PranayKanathala_NYCSchools

class _0230214_PranayKanathala_NYCSchoolsTests: XCTestCase {
    var nycSchoolsData: [HighSchoolModel]?
    var nycSchoolDetailData: [SchoolDetailModel]?

    override func setUpWithError() throws {
        // Load Stub
        if let data = loadStub(name: "NYCSchoolsData") {
            // Create JSON Decoder
            let decoder = JSONDecoder()
            // Configure JSON Decoder
            decoder.dateDecodingStrategy = .secondsSince1970
            // Decode JSON
            let schoolData = try decoder.decode([HighSchoolModel].self, from: data)
            // Initialize View Model
            self.nycSchoolsData = schoolData
        }
        
        // Load Stub
        if let data = loadStub(name: "NYCSchoolsDetailData") {
            // Create JSON Decoder
            let decoder = JSONDecoder()
            // Configure JSON Decoder
            decoder.dateDecodingStrategy = .secondsSince1970
            // Decode JSON
            let detailData = try decoder.decode([SchoolDetailModel].self, from: data)
            // Initialize View Model
            self.nycSchoolDetailData = detailData
        }
    }

    // MARK: - Tests for Visibility
    func testMoviePage() {
        XCTAssertEqual(nycSchoolsData?.first?.dbn, Stub.dbnValue)
    }
    
    func testSchoolName() {
        XCTAssertEqual(nycSchoolsData?[9].school_name, Stub.schoolName)
    }
    
    func testSatTestTakers() {
        XCTAssertEqual(nycSchoolDetailData?[0].num_of_sat_test_takers, Stub.satTestTakers)
    }
}

extension _0230214_PranayKanathala_NYCSchoolsTests {
    struct Stub {
        static let dbnValue = "02M260"
        static let schoolName = "Richmond Hill High School"
        static let satTestTakers = "29"

    }
}
