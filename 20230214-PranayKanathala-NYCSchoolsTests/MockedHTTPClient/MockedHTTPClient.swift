//
//  File.swift
//  20230214-PranayKanathala-NYCSchoolsTests
//
//  Created by Pet on 2/14/23.
//

import XCTest
@testable import _0230214_PranayKanathala_NYCSchools

class MockedHTTPClient: DataManager {
    class func getData(page: Int, searchQuery: String?, onSuccess: @escaping (Data?) -> Void,
                       onError: @escaping APIErrorHandler) {
        guard let url = Bundle(for: MockedHTTPClient.self).url(forResource: "NYCSchoolsData", withExtension: "json"),
              let data = try? Data(contentsOf: url)
        else {
            return onError(RestError(400, "Unknown"))
        }
        onSuccess(data)
    }
}

